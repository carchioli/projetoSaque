package com.itau.saqueInternacional;


import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main( String[] args )
	{

		String url = "http://data.fixer.io/api/latest?access_key=578ac533a860ac0f3d508e3dbad0ef57";
		String pathAccounts = "src/main/accounts.csv";
		String pathWithdrals = "src/main/withdrawals.csv";

		StringBuilder resultadoSaques = new StringBuilder();
		List<String> contas;
		List<String> saques;
		StringBuilder arquivoSaida = new StringBuilder();


		RatesResponse cotacoes = JsonConverter.getContacoes(ApiRequester.getArquivoCotacoes(url));

		try {
			contas = FileManager.getConteudo(pathAccounts);
			saques = FileManager.getConteudo(pathWithdrals);


			//Contas: name,username,password,balance,type
			//Saques: username,amount,currency

			for(int i=1;i<saques.size();i++) {

				String[] linhaSaque = saques.get(i).split(",");

				boolean findAccount = false;
				int indexConta = 1;
				while(!findAccount) {

					String[] linhaConta = contas.get(indexConta).split(",");

					if(linhaConta[1].equals(linhaSaque[0])) {
						findAccount = true;

						double saldo = Double.parseDouble(linhaConta[3]);
						double saque = Double.parseDouble(linhaSaque[1]);

						//converte para euro
						saque = cotacoes.converteMoeda(linhaSaque[2], saque);

						//Aplica taxa de saque para basic
						if(linhaConta[4].equals("basic")) {
							saque = saque * 1.025;
						}

						if(saldo> saque) {

							saldo = saldo - saque;

							String linha = linhaConta[0] + "," + linhaConta[1] + "," + linhaConta[2] + "," + saldo + "," + linhaConta[4]; 

							System.out.println(linha);
							System.out.println("======================");

							contas.set(indexConta, linha);

						}else {
							System.out.println("Saldo Insuficiente!");
						}
					}
					indexConta++;
				}
			}


			for(int j=0;j<contas.size();j++) {
				String linha = contas.get(j);
				String[] partes = linha.split(",");			
				arquivoSaida.append(partes[0] + "," + partes[1] + "," + partes[2] + "," + partes[3] + "," + partes[4] + "\n");
			}


			System.out.println(arquivoSaida);

		}catch(Exception e) {
			System.out.println("Erro na leitura dos arquivos");
			e.printStackTrace();
		}    	

	}
}
