package com.itau.saqueInternacional;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FileManager {
	
	private static List<String> lista;
	
	public static List getConteudo(String arquivo) throws Exception{
		
		Path path = Paths.get(arquivo);
		lista = Files.readAllLines(path);
		
		return lista;
	}
	
}
