package com.itau.saqueInternacional;

import com.google.gson.Gson;

public class JsonConverter {
	
	
	public static RatesResponse getContacoes(String json) {
		
		Gson gson = new Gson();
	
		RatesResponse response = gson.fromJson(json, RatesResponse.class);
		
		return response;
		
	}
	
}
