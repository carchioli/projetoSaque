package com.itau.saqueInternacional;

import java.util.HashMap;

public class RatesResponse {

	public boolean sucecess;
	public int timestemp;
	public String base;
	public String date;
	public HashMap<String, Double> rates;
	public boolean isSucecess() {
		return sucecess;
	}
	public void setSucecess(boolean sucecess) {
		this.sucecess = sucecess;
	}
	public int getTimestemp() {
		return timestemp;
	}
	public void setTimestemp(int timestemp) {
		this.timestemp = timestemp;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public HashMap<String, Double> getRates() {
		return rates;
	}
	public void setRates(HashMap<String, Double> rates) {
		this.rates = rates;
	}


	public Double converteMoeda(String moeda, double valor) {

		double conversao = 0;
		double cotacao = 0;

		cotacao = rates.get(moeda);

		conversao = valor/cotacao;


		return conversao;

	}



}
