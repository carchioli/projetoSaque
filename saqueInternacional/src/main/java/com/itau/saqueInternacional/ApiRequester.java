package com.itau.saqueInternacional;

import com.github.kevinsawicki.http.HttpRequest;

public class ApiRequester {

	public static String getArquivoCotacoes(String url) {

		String arquivo = "";

		arquivo = HttpRequest.get(url).body();

		return arquivo;
	}

}
